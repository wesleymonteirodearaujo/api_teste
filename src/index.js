const { gql, ApolloServer } = require("apollo-server");
const mysql = require("mysql2/promise");
const dotenv = require("dotenv");

dotenv.config();

const dbConfig = {
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
};

async function executeQuery(sql, params) {
  const connection = await mysql.createConnection(dbConfig);
  const [rows] = await connection.execute(sql, params);
  connection.end();
  return rows;
}

const typeDefs = gql`
  type User {
    id: ID!
    nome: String
    email: String
    senha: String
  }

  type Post {
    id: ID!
    autor: String
    texto: String
    data: String
  }

  type Produto {
    id: Int!
    categoria: String
    nome: String
    preco: Float
    descricao: String
  }

  type Tarefa {
    id: Int!
    user_id: Int
    titulo: String
    data: String
  }

  type Livro {
    id: ID!
    autor: String
    titulo: String
    genero: String
    data: String
  }

  type Evento {
    id: ID!
    latitude: Int!
    longitude: Int!
    data: String!
    nome: String!
    descricao: String!
    horario: String!
  }

  type Pedido {
    id: ID!
    status: String!
    numero_pedido: Int!
    itens: String!
    data: String!
  }

  type Query {
    users: [User]
    user(id: ID!): User
    posts: [Post]
    post(id: ID!): Post
    produtos(categoria: String): [Produto]
    tarefas(user_id: Int): [Tarefa]
    tarefa(id: Int!): Tarefa
    produto(id: Int!): Produto
    livros: [Livro]
    livro(autor: String): [Livro]  # Corrigido para retornar uma lista de livros
    eventos: [Evento]
    evento(latitude: Int!, longitude: Int!, data: String!): Evento
    pedidos: [Pedido]
    pedido(status: String!): [Pedido]
  }

  type Mutation {
    createUser(nome: String!, email: String!, senha: String!): User
    deleteUser(id: ID!): Boolean
    updateUser(id: ID!, nome: String, email: String, senha: String): User

    createPost(autor: String!, texto: String!, data: String!): Post
    deletePost(id: ID!): Boolean
    updatePost(id: ID!, autor: String, texto: String, data: String): Post

    createProduto(categoria: String!, nome: String!, preco: Float!, descricao: String!): Produto
    deleteProduto(id: Int!): Boolean
    updateProduto(id: Int!, categoria: String, nome: String, preco: Float, descricao: String): Produto

    createTarefa(user_id: Int!, titulo: String!, data: String!): Tarefa
    deleteTarefa(id: Int!): Boolean
    updateTarefa(id: Int!, user_id: Int, titulo: String, data: String): Tarefa

    createLivro(autor: String!, titulo: String!, genero: String!, data: String!): Livro
    deleteLivro(id: ID!): Boolean
    updateLivro(id: ID!, autor: String, titulo: String, genero: String, data: String): Livro

    createEvento(latitude: Int!, longitude: Int!, data: String!, nome: String!, descricao: String!, horario: String!): Evento
    updateEvento(id: ID!, latitude: Int, longitude: Int, data: String, nome: String, descricao: String, horario: String): Evento
    deleteEvento(id: ID!): Boolean

    criarPedido(status: String!, numero_pedido: Int!, itens: String!, data: String!): Pedido
    atualizarPedido(id: ID!, status: String, numero_pedido: Int, itens: String, data: String): Pedido
    deletarPedido(id: ID!): Boolean
  }
`;

const resolvers = {
  Query: {
    users: async () => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM users");
      connection.end();
      return rows;
    },
    user: async (_, { id }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM users WHERE id = ?", [id]);
      connection.end();
      return rows[0];
    },
    posts: async () => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM posts");
      connection.end();
      return rows;
    },
    post: async (_, { id }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM posts WHERE id = ?", [id]);
      connection.end();
      return rows[0];
    },
    produtos: async () => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM produtos");
      connection.end();
      return rows;
    },
    produto: async (_, { id }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM produtos WHERE id = ?", [id]);
      connection.end();
      return rows[0];
    },
    tarefas: async () => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM tarefas");
      connection.end();
      return rows;
    },
    tarefa: async (_, { id }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM tarefas WHERE user_id = ?", [id]);
      connection.end();
      return rows[0];
    },
    livros: async () => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM livros");
      connection.end();
      return rows;
    },
    livro: async (_, { autor }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM livros WHERE autor = ?", [autor]);
      connection.end();
      return rows;
    },
    eventos: async () => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM eventos");
      connection.end();
      return rows;
    },
    evento: async (_, { latitude, longitude, data }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM eventos WHERE latitude = ? AND longitude = ? AND data = ?", [latitude, longitude, data]);
      connection.end();
      return rows.length > 0 ? rows[0] : null;
    },
    pedidos: async () => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM pedidos");
      connection.end();
      return rows;
    },
    pedido: async (_, { status }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [rows] = await connection.execute("SELECT * FROM pedidos WHERE status = ?", [status]);
      connection.end();
      return rows;  // Alterado para retornar rows diretamente
    },
  },
  Mutation: {
    createUser: async (_, { nome, email, senha }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [result] = await connection.execute("INSERT INTO users (nome, email, senha) VALUES (?, ?, ?)", [
        nome,
        email,
        senha,
      ]);
      connection.end();
      return { id: result.insertId, nome, email, senha };
    },
    deleteUser: async (_, { id }) => {
      const connection = await mysql.createConnection(dbConfig);
      await connection.execute("DELETE FROM users WHERE id = ?", [id]);
      connection.end();
      return true;
    },
    updateUser: async (_, { id, nome, email, senha }) => {
      const connection = await mysql.createConnection(dbConfig);
      await connection.execute("UPDATE users SET nome = ?, email = ?, senha = ? WHERE id = ?", [
        nome,
        email,
        senha,
        id,
      ]);
      connection.end();
      return { id, nome, email, senha };
    },
    createPost: async (_, { autor, texto, data }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [result] = await connection.execute("INSERT INTO posts (autor, texto, data) VALUES (?, ?, ?)", [
        autor,
        texto,
        data,
      ]);
      connection.end();
      return { id: result.insertId, autor, texto, data };
    },
    deletePost: async (_, { id }) => {
      const connection = await mysql.createConnection(dbConfig);
      await connection.execute("DELETE FROM posts WHERE id = ?", [id]);
      connection.end();
      return true;
    },
    updatePost: async (_, { id, autor, texto, data }) => {
      const connection = await mysql.createConnection(dbConfig);
      await connection.execute("UPDATE posts SET autor = ?, texto = ?, data = ? WHERE id = ?", [autor, texto, data, id]);
      connection.end();
      return { id, autor, texto, data };
    },
    createProduto: async (_, { categoria, nome, preco, descricao }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [result] = await connection.execute(
        "INSERT INTO produtos (categoria, nome, preco, descricao) VALUES (?, ?, ?, ?)",
        [categoria, nome, preco, descricao]
      );
      connection.end();
      return { id: result.insertId, categoria, nome, preco, descricao };
    },
    deleteProduto: async (_, { id }) => {
      const connection = await mysql.createConnection(dbConfig);
      await connection.execute("DELETE FROM produtos WHERE id = ?", [id]);
      connection.end();
      return true;
    },
    updateProduto: async (_, { id, categoria, nome, preco, comentario }) => {
      const connection = await mysql.createConnection(dbConfig);
      await connection.execute(
        "UPDATE produtos SET categoria = ?, nome = ?, preco = ?, comentario = ? WHERE id = ?",
        [categoria, nome, preco, comentario, id]
      );
      connection.end();
      return { id, categoria, nome, preco, comentario };
    },
    createTarefa: async (_, { user_id, titulo, data }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [result] = await connection.execute(
        "INSERT INTO tarefas (user_id, titulo, data) VALUES (?, ?, ?)",
        [user_id, titulo, data]
      );
      connection.end();
      return { id: result.insertId, user_id, titulo, data };
    },
    deleteTarefa: async (_, { id }) => {
      const connection = await mysql.createConnection(dbConfig);
      await connection.execute("DELETE FROM tarefas WHERE id = ?", [id]);
      connection.end();
      return true;
    },
    updateTarefa: async (_, { id, user_id, titulo, data }) => {
      const connection = await mysql.createConnection(dbConfig);
      await connection.execute("UPDATE tarefas SET user_id = ?, titulo = ?, data = ? WHERE id = ?", [
        user_id,
        titulo,
        data,
        id,
      ]);
      connection.end();
      return { id, user_id, titulo, data };
    },
    createLivro: async (_, { autor, titulo, genero, data }) => {
      const connection = await mysql.createConnection(dbConfig);
      const [result] = await connection.execute("INSERT INTO livros (autor, titulo, genero, data) VALUES (?, ?, ?, ?)", [
        autor,
        titulo,
        genero,  // Adicionei o parâmetro que estava faltando
        data,
      ]);
      connection.end();
      return { id: result.insertId, autor, titulo, genero, data };
    },
    deleteLivro: async (_, { id }) => {
      const connection = await mysql.createConnection(dbConfig);
      await connection.execute("DELETE FROM livros WHERE id = ?", [id]);
      connection.end();
      return true;
    },
    updateLivro: async (_, { id, autor, titulo, data }) => {
      const connection = await mysql.createConnection(dbConfig);
      await connection.execute("UPDATE livros SET autor = ?, titulo = ?, data = ? WHERE id = ?", [
        autor,
        titulo,
        data,
        id,
      ]);
      connection.end();
      return { id, autor, titulo, data };
    },
    createEvento: async (_, { latitude, longitude, data, nome, descricao, horario }) =>
    executeQuery(
      "INSERT INTO eventos (latitude, longitude, data, nome, descricao, horario) VALUES (?, ?, ?, ?, ?, ?)",
      [latitude, longitude, data, nome, descricao, horario]
    ).then((result) => ({ id: result.insertId, latitude, longitude, data, nome, descricao, horario })),
    updateEvento: async (_, { id, latitude, longitude, data, nome, descricao, horario }) =>
      executeQuery(
        "UPDATE eventos SET latitude = ?, longitude = ?, data = ?, nome = ?, descricao = ?, horario = ? WHERE id = ?",
        [latitude, longitude, data, nome, descricao, horario, id]
      ).then(() => ({ id, latitude, longitude, data, nome, descricao, horario })),
    deleteEvento: async (_, { id }) => executeQuery("DELETE FROM eventos WHERE id = ?", [id]).then(() => true),

    criarPedido: async (_, { status, numero_pedido, itens, data }) => {
      const result = await executeQuery(
        "INSERT INTO pedidos (status, numero_pedido, itens, data) VALUES (?, ?, ?, ?)",
        [status, numero_pedido, itens, data]
      );
      return {
        id: result.insertId,
        status,
        numero_pedido,
        itens,
        data,
      };
    },

    atualizarPedido: async (_, { id, status, numero_pedido, itens, data }) => {
      await executeQuery(
        "UPDATE pedidos SET status = ?, numero_pedido = ?, itens = ?, data = ? WHERE id = ?",
        [status, numero_pedido, itens, data, id]
      );
      return {
        id,
        status,
        numero_pedido,
        itens,
        data,
      };
    },

    deletarPedido: async (_, { id }) => {
      await executeQuery("DELETE FROM pedidos WHERE id = ?", [id]);
      return true;
    },
  },
};

const server = new ApolloServer({ typeDefs, resolvers });

server.listen().then(({ url }) => console.log(`Server running on ${url}`));
